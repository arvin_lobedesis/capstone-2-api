const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
//configure the dotenv package to access environment variables via the process.env object
require('dotenv').config()
const port = process.env.PORT

//whitelist the port where our NextJS client app will be served from
// var corsOptions = {
//     origin: ['http://localhost:3000',
//             'https://capstone2-budget-tracking-app.vercel.app',
//             'https://capstone2-budget-tracking-app.arvin-lobedesis.vercel.app',
//             'https://capstone2-budget-tracking-app-git-master.arvin-lobedesis.vercel.app',
//             'https://capstone2-budget-tracking-app-4tur4hgma.vercel.app'],
//     optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }
app.use(cors());
//connect to mongodb atlas cloud database
mongoose.connect(process.env.DB_MONGODB, { 
	useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false 
})

//set notifications for connection status
let db = mongoose.connection 
//if connection error is encountered, output error in the console
db.on('error', () => {
    console.log('oops something went wrong with our MongoDB connection')
})

//once connected, notify as well
db.once('open', () => {
    console.log('connected to database')
})

//enable processing of JSON request bodies
app.use(express.json())
//enable processing of form data
app.use(express.urlencoded({extended:true}))

//import routes
const userRoutes = require('./routes/user')
const budgetRoutes = require('./routes/budget')
const categoryRoutes = require('./routes/category')

//assign the imported routes to their respective endpoints
app.use('/api/users', userRoutes)
app.use('/api/budgets', budgetRoutes)
app.use('/api/categories', categoryRoutes)


app.listen(process.env.PORT, () => {
    console.log(`API is now online on port ${process.env.PORT}`)
})