const Budget = require('../models/budget')
const User = require('../models/user')


module.exports.getAll = () => {
    //modify to instead return all budgets regardless of active status
    return Budget.find()
    .then(budgets => budgets)
}

module.exports.getOne = (params) => {
    return Budget.findById(params.id)
    .then((budget, err) => {
        if(err) return false
        return budget
    })
}

//controller for creating a budget (only registered users may do so)
module.exports.addBudget = (params) => {
    
        const budget = new Budget({
            name: params.name,
            categoryId: params.categoryId,
            type: params.type,
            amount: params.amount,
            description: params.description,
            
            
        })
    
        return budget.save()
        .then((updatedBudget, err) => {
            console.log(updatedBudget)
            if(err) return false
            return User.findById(params.userId)
            .then((user, err)=>{
                if(err) return false
                user.budgets.push({
                    _id: updatedBudget._id,
                })
                return user.save()
                .then((user, err)=>{
                    if(err) return false
                    // console.log("success",user._id)
                    return user._id
                })
            })
        })
    
}


module.exports.editBudget = (params) => {
    return Budget.findByIdAndUpdate(params.budgetId, params.reqBody, {new: true})
    .then((budget, err) => {
		if (err) return false
		return User.findById(params.user.id).populate({ 
			path: 'budgets',
			populate: {
				path: 'categoryId',
				model: 'category'
			}}).then(user => {
			return user
		})
	})
}




module.exports.archive = (params) => {
    
        return User.findById(params.userId)
        .populate({
            path: 'budgets',
            populate: {
                path: 'categoryId',
                model: 'category'
            }
        })
        .then((user, err) => {
            if(err) return false
            return Budget.findById(params.budgetId)
            .then((budget, err) => {
                console.log(budget, 'budget')
                if(budget.isActive===true){
                    budget.isActive = false
                    if(budget.type==='Expense'){
                        user.balance = user.balance + + budget.amount
                    }else{
                        user.balance = user.balance - + budget.amount
                    }
                }else{
                    budget.isActive = true
                    if(budget.type === 'Expense'){
                        user.balance = + user.balance - + budget.amount
                    }else{
                        user.balance = + user.balance + + budget.amount
                    }
                }
                return budget.save()
                .then((updatedBudget, err) => {
                    // console.log(updatedBudget, 'updatedBudget')
                    if(err) return false
                    return user.save()
                    .then((user, err) => {
                        if(err) return false
                        // console.log(user.balance, 'User')
                        return user
                    })
                })
            })
        })
}