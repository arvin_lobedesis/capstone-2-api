const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = '528896774469-h2h71im6b9es2k3l4act6iruasqb3qhk.apps.googleusercontent.com'


module.exports.index = (params) => {
	return User.find({}).then((resolve, reject) => {
		return reject ? false : resolve
	})
}


module.exports.register = (params) => {
    return User.findOne({email: params.email})
    .then(email => {
        if(email !== null){
            return false
        }else{
            const user = new User({
                userName: params.userName,
                email: params.email,
                password: bcrypt.hashSync(params.password, 10),
                isAdmin: params.isAdmin
            })
        
            return user.save()
            .then((user, err) => {
                return (err) ? false : true
            })
        }
    })
    
}

module.exports.login = (params) => {
    return User.findOne({email: params.email})
    .then(user => {
        if(user === null){
            return false
        }
        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if(isPasswordMatched){
            return{
                accessToken: auth.createAccessToken(user.toObject())
            }
        }else{
            return false
        }
    })
}


module.exports.getOne = (params) => {
    return User.findById(params.userId).populate({
        path:'budgets',
        populate: {
            path: 'categoryId',
            model: 'category'
        }
    })
    .then(userData => {
		userData.password = undefined
		return userData
	})
}



module.exports.verifyGoogleTokenId = async(tokenId) => {
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
    if(data.payload.email_verified === true){
        const user = await User.findOne({email: data.payload.email}).exec()
        if(user !== null){
            if(user.loginType === 'google'){
                return {accessToken: auth.createAccessToken(user.toObject())}
            }else{
                return {error: 'login-type-error'}
            }
        }else{
            const newUser = new User ({
                userName:data.payload.given_name,
                email:data.payload.email,
                loginType: 'google'
            })

            return newUser.save().then((user, err)=>{
                console.log(user)
                return {accessToken: auth.createAccessToken(user.toObject())}
            })
        }

    }else{
        return {error: 'google-auth-error'}
    }
}

module.exports.editBalance = (params) => {
	return User.findByIdAndUpdate(params.userId, {
		balance: params.balance
	}, {new:true}).populate({
		path: 'budgets',
		populate: {
			path: 'categoryId',
			model: 'category'
		}
	}).then((user, error) => {
		if(error) return false
			console.log(user)
		return user
	})
}
