const express = require('express')
const router = express.Router()
const BudgetController = require('../controllers/budget')
const auth = require('../auth')

router.get('/', (req, res) => {
    BudgetController.getAll().then(budgets => res.send(budgets))
})

router.get('/:id', (req, res) => {
    BudgetController.getOne({id: req.params.id}).then(budget => res.send(budget))
})


router.post('/', auth.verify,  (req, res) => {
    const arg = {
        
        name: req.body.name,
        categoryId: req.body.categoryId,
        type: req.body.type,
        amount: req.body.amount,
        balance: req.body.balance,
        description: req.body.description,
        userId: auth.decode(req.headers.authorization).id
    }
    BudgetController.addBudget(arg).then(result => res.send(result))
})

router.delete('/:budgetId', auth.verify, (req,res) => {
    // console.log(auth.decode(req.headers.authorization).id)
    const arg = {
        budgetId: req.params.budgetId,
        userId: auth.decode(req.headers.authorization).id
    }
    BudgetController.archive(arg).then(result => res.send(result))
})

router.put('/:budgetId', auth.verify, (req, res) => {
    
    const arg = {
        budgetId: req.params.id,
        reqBody: req.body,
        user: auth.decode(req.headers.authorization)
        
        
    }
    BudgetController.update(arg).then(result => res.send(result))
})

module.exports = router