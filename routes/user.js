const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')
const auth = require('../auth')

router.get('/', (req, res) => {
	UserController.index().then(result => res.send(result))
})

router.post('/register', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})

router.get('/details',auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.getOne({userId: user.id}).then(result => res.send(result))
})


router.post('/verify-google-id-token', async(req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

router.patch('/:userId', auth.verify, (req, res) => {
	const arg = {
		userId: req.params.userId,
		balance: req.body.balance
	}
	UserController.editBalance(arg).then(result => res.send(result))
})





module.exports = router