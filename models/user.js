const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: [true, 'Username is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    balance: {
        type: Number,
        default: 0
    },
    loginType: {
        type: String,
        default: 'email'
    },
    budgets: [
        {
            type: Schema.Types.ObjectId,
		    ref: 'budget'
        }
    ]
    

})

module.exports = mongoose.model('user', userSchema)