const mongoose = require('mongoose')


const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true,'Add Category name']
    },
    isActive: {
        type: Boolean,
        default: true
    }

})

module.exports = mongoose.model('category', categorySchema)