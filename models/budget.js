const mongoose = require('mongoose')
const Schema = mongoose.Schema

const budgetSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Budget Name is required']
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'category'
    },
    type: {
        type: String,
        required: [true, 'Category type is required']
    },
    amount: {
        type: Number,
        required: [true, 'Amount is required']
    },
    description: {
        type: String
    },
    dateAdded: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean,
        default: true
    }
    
})

module.exports = mongoose.model('budget', budgetSchema)